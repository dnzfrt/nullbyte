import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {
  List,
  Content,
  ListItem,
  Text,
  Body,
  Button as Btn,
} from 'native-base';
import axios from 'axios';
import { Col, Grid } from 'react-native-easy-grid';


export default class History extends Component {
  state = { data: {}, loading: true };


  getOrders() {
    const url = `http://10.0.2.2:3000/orders`;
    axios.get(url).then(response => response.data)
      .then((data) => {
        this.setState({ data: data })
        this.setState({ loading: false })
      });
  }
  componentDidMount() {
    this.getOrders();
  }

  render() {
    return (
      <Content>
        <List
          style={styles.list}
          dataArray={this.state.data}
          renderRow={(data) =>
            <ListItem>
              <Body>
                <Grid>
                  <Col>
                    <Text>{data.name}</Text>
                    <Text note>{data.total} TL</Text>
                  </Col>
                </Grid>
              </Body>
            </ListItem>
          }>
        </List>
      </Content>
    )
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  list: {
    backgroundColor: '#fff'
  },
});