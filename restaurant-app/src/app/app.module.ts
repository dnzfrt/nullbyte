import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from  '@angular/common/http';
import { ItemsListComponent } from './items-list/items-list.component';
import { AddItemComponent } from './add-item/add-item.component';
import { FormsModule } from '@angular/forms';
import { EditItemComponent } from './edit-item/edit-item.component';
import { UserListComponent } from './user-list/user-list.component';
import { OrdersListComponent } from './orders-list/orders-list.component';
import { PipePipe } from './pipe.pipe';


@NgModule({
  declarations: [
    AppComponent,
    ItemsListComponent,
    AddItemComponent,
    EditItemComponent,
    UserListComponent,
    OrdersListComponent,
    PipePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
