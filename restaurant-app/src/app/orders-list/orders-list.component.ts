import { Component, OnInit } from '@angular/core';
import { HttpClient } from  "@angular/common/http";
import { Router } from '@angular/router';
import { OrderService } from '../order.service';


@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.css']
})
export class OrdersListComponent implements OnInit {
  orders = [] ;
  total :  number;
  keys : Array<string>;

  constructor(private orderService: OrderService,
    private router: Router) { }

  ngOnInit(): void {
    this.loadOrders()
  }
  loadOrders(){
    this.orderService.getOrders()
      .subscribe((data:Array<any> = [])=>{
        this.orders=data;
      }   
      );
  }
  getKeys(obj: any): Array<string> {
    return Object.keys(obj);
  }
  getTotal(){
    let total = 0;
    for(let i = 0 ; i < this.orders.length;i++){
      total = total + this.orders[i].total;
    }
    this.total = total;
  }
  
  deleteOrder(id){
    this.orderService.delete(id)
    .subscribe(
      response => {
        console.log(response);
        window.location.reload();
      },
      error => {
        console.log(error);
      });
  }

}
