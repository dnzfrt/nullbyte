import { Component, OnInit } from '@angular/core';
import { HttpClient } from  "@angular/common/http";
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users = [] ;

  constructor(private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
    this.loadUsers()
  }
  loadUsers(){
    this.userService.getUsers()
      .subscribe((data:any[])=>{
        console.log(data);
        this.users=data; 
      }   
      );
  }
  deleteUser(id){
    this.userService.delete(id)
    .subscribe(
      response => {
        console.log(response);
        window.location.reload();
      },
      error => {
        console.log(error);
      });
  }
}
